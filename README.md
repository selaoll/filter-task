# Notes about the assignment

Unfortunately did not have too much time to dedicate for this task, which might show.. ^_^

For example: 

* Mostly just the one component for simplicity's sake
* No `<form>`, probably should use..
* Naive way of binding data and creating the result


Task description:

```
Please create an empty HTML page that has a header with the Shpock Logo and a Search input field.


When the user starts typing in the search field, a "Filters" overlay should appear from under the search field and show the detailed filtering options. Please use the Shpock mobile app to see all filtering options currently available.


On submission of the form:

- gather all the selected filter data from the form and display it in a JSON object in the body of the same page and in the browser console

- hide the Filters overlay


You can use the attached mockup to get some inspiration on how to structure the dropdown block.


To accomplish the task you are free to use any frontend tools you want (frameworks or libraries for JS or CSS).
```
