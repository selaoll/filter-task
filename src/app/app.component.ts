import {Component, OnInit} from '@angular/core';
import {SearchCategory} from './search-category';
import {SearchCategoryService} from './search-category.service';
import {SearchRequest} from './search-request';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  showFilters = false;
  categories: SearchCategory[];
  searchInput: string;
  radius?: number;
  maxAgeDays?: number;

  formData?: SearchRequest;

  constructor(private searchCategoryService: SearchCategoryService) {
  }

  ngOnInit(): void {
    this.categories = this.searchCategoryService.getCategories();
  }

  public onSubmit(): void {
    this.formData = this.gatherFormData();
    console.log('Form:', this.formData);
    this.showFilters = false;
  }

  get noCategoriesSelected(): boolean {
    return !this.categories.some(category => category.selected);
  }

  clearCategories(event): void {
    event.preventDefault(); // prevent uncheck
    this.categories.forEach(category => category.selected = false);
  }

  private get selectedCategoryIds(): string[] {
    return this.categories.filter(category => category.selected).map(category => category.id);
  }

  private gatherFormData(): SearchRequest {
    return {
      text: this.searchInput,
      categories: this.selectedCategoryIds,
      radius: this.radius,
      maxAgeDays: this.maxAgeDays
    };
  }
}

