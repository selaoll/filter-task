import {Injectable} from '@angular/core';

@Injectable()
export class SearchCategoryService {

  private defaultLabels = [
    'New in your area',
    'Home and garden',
    'Fashion and Accessories',
    'Electronics',
    'Baby and Child',
    'Sport, Leisure and Games',
    'Movies, Books and Music',
    'Cars and Motors',
    'Property',
    'Services',
    'Other',
  ];

  constructor() {
  }

  private static createFakeId(label: string) {
    return label.toLowerCase().replace(/\s/g, '_').replace(',', '');
  }

  /**
   * Get some "fake" categories
   */
  public getCategories() {
    return this.defaultLabels.map(label => Object.assign({}, {
      label: label,
      id: SearchCategoryService.createFakeId(label)
    }));
  }


}
