export interface SearchCategory {
  id: string;
  label: string;
  selected?: boolean;
}
