import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatIconModule, MatSliderModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {SearchCategoryService} from './search-category.service';
import {RangeSliderComponent} from './range-slider/range-slider.component';


const MATERIAL_MODULES = [
  MatIconModule,
  MatButtonModule,
  MatCheckboxModule,
  MatSliderModule
];

@NgModule({
  declarations: [
    AppComponent,
    RangeSliderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MATERIAL_MODULES
  ],
  providers: [
    SearchCategoryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
