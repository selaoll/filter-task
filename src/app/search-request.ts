export interface SearchRequest {
  text: string;
  categories: string[];
  radius: number;
  maxAgeDays: number;
}
