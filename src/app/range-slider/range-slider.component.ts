import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-range-slider',
  templateUrl: './range-slider.component.html',
  styleUrls: ['./range-slider.component.scss']
})
export class RangeSliderComponent implements OnInit {

  @Input()
  range: any[];

  @Input()
  maxValueLabel;

  @Input()
  affix: string;

  @Input()
  label: string;

  sliderValue: number;

  @Output()
  changed = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
    this.sliderValue = this.max;
  }

  get max() {
    return this.range.length - 1;
  }

  get value() {
    return this.range[this.sliderValue];
  }

  get displayText() {
    return this.sliderValue === this.max && this.maxValueLabel || `${this.value} ${this.affix}`;
  }

  sliderMoved(value) {
    this.sliderValue = value;
    this.changed.emit(this.value);
  }

}
